class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    word_length = referee.pick_secret_word
    guesser.register_secret_length(word_length)
    @board = Array.new(word_length, nil)
  end

  def take_turn
    guess = guesser.guess(@board)
    matches = referee.check_guess(guess)
    update_board(matches, guess)
    guesser.handle_response(guess, matches)
  end

  def update_board(matches, guess)
    matches.each { |idx| @board[idx] = guess }
  end

end

class HumanPlayer
end

class ComputerPlayer

  attr_reader :dictionary, :secret_word, :candidate_words


  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def check_guess(guess)
    results = secret_word.chars.map.with_index do |ch, idx|
      idx if guess == ch
    end
    results.compact
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    counter = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |let, idx|
        counter[word[idx]] += 1 if let == nil
      end
    end
    letters = counter.sort_by { |k, v| v }
    letters[-1][0]
  end

  def handle_response(guess, matched_idx)
    @candidate_words = candidate_words.select do |word|
      matched_idx == check_index(guess, word)
    end
  end

  def check_index(ch, word)
    word.chars.map.with_index { |char, idx| idx if char == ch }.compact
  end

end
